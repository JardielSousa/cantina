package br.com.jas.bandeco.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.jas.bandeco.representacao.FormAlterarSenha;

@Component
public class FormAlterarSenhaValidator implements Validator {

//	@Autowired
//	private UsuarioService usuarioService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return FormAlterarSenha.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
//		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "senhaAtual", "field.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "novaSenha", "field.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "confirmarNovaSenha", "field.empty");
		
		FormAlterarSenha obj = (FormAlterarSenha) target;
//		String email = obj.getEmail();
//		String senhaAtual = obj.getSenhaAtual();
		String novaSenha = obj.getNovaSenha();
		String confirmarNovaSenha = obj.getConfirmarNovaSenha();
		
//		Boolean mesmaSenha = this.usuarioService.isMesmaSenha(senhaAtual, email);
//		if (!(mesmaSenha)) {
//			errors.rejectValue("senhaAtual", "senha.atual.diferente");
//		}
		
		if (!(novaSenha.equals(confirmarNovaSenha))) {
			errors.rejectValue("confirmarNovaSenha", "confirmarSenha.notEqualsSenha");
		}
	}

}
