package br.com.jas.bandeco.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.model.Usuario;
import br.com.jas.bandeco.service.UsuarioService;

@Component
public class PerfilValidator implements Validator {

	@Autowired
	private UsuarioService usuarioService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Perfil.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "nome.empty");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario.email", "email.empty");
		
		Perfil p = (Perfil) target;
		Usuario u = p.getUsuario();

		String senha = (u.getSenha() == null) ? "" : u.getSenha().trim();
		String confirmarSenha = (u.getConfirmarSenha() == null) ? "" : u.getConfirmarSenha().trim();
		
		if (p.getId() == null) {
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario.senha", "senha.empty");
			ValidationUtils.rejectIfEmptyOrWhitespace(errors, "usuario.confirmarSenha", "confirmarSenha.empty");
			boolean existsEmail = this.usuarioService.existsById(u.getEmail());
			if (existsEmail) {
				errors.rejectValue("usuario.email", "email.registered");
			}
		} else if (senha.length() > 0) {
			if (confirmarSenha.isEmpty()) {
				errors.rejectValue("usuario.confirmarSenha", "confirmarSenha.empty");
			} else if (!(senha.equals(confirmarSenha))) {
				errors.rejectValue("usuario.confirmarSenha", "confirmarSenha.notEqualsSenha");
			}
		}
	}

}
