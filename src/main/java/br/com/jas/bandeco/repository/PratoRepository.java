package br.com.jas.bandeco.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.jas.bandeco.model.Prato;

public interface PratoRepository extends JpaRepository<Prato, Long> {

}
