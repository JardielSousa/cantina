package br.com.jas.bandeco.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import br.com.jas.bandeco.model.Perfil;

public interface PerfilRepository extends JpaRepository<Perfil, Long>, PerfilRepositoryCustom {

	@Query("SELECT p FROM Perfil p WHERE p.usuario.email=?1")
	public Perfil findByEmail(String email);
	
}
