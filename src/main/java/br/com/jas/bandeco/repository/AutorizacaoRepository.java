package br.com.jas.bandeco.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.jas.bandeco.enumeration.TipoRole;
import br.com.jas.bandeco.model.Autorizacao;

@Repository
@Transactional
public interface AutorizacaoRepository extends JpaRepository<Autorizacao, Long> {

	@Modifying
	@Query("UPDATE Autorizacao a SET a.ativo=false WHERE a=:aut")
	public Integer desabilitar(@Param("aut") Autorizacao a);

	@Modifying
	@Query("UPDATE Autorizacao a SET a.ativo=true WHERE a=:aut")
	public Integer habilitar(@Param("aut") Autorizacao a);

	@Modifying
	@Query("UPDATE Autorizacao a SET a.role=:role WHERE a=:aut")
	public Integer editarRole(@Param("role") TipoRole role, @Param("aut") Autorizacao a);

}
