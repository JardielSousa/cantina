package br.com.jas.bandeco.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import br.com.jas.bandeco.model.Usuario;

@Repository
@Transactional
public interface UsuarioRepository extends JpaRepository<Usuario, String> {

	@Query("SELECT u.senha FROM Usuario u WHERE u.email=?1")
	public String senha(String email);

	@Modifying
	@Query("UPDATE Usuario u SET u.senha=?1 WHERE u.email=?2")
	public Integer atualizarSenha(String novaSenha, String email);
	
}
