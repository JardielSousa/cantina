package br.com.jas.bandeco.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.model.Usuario;

@Transactional
public class PerfilRepositoryCustomImpl implements PerfilRepositoryCustom {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public Integer editar(Perfil perfil) {
		Usuario usuario = perfil.getUsuario();
		String qlStringUsuario = "UPDATE Usuario u SET u.ativo=:ativo WHERE u.email=:email";
		this.em.createQuery(qlStringUsuario)
				.setParameter("ativo", usuario.getAtivo())
				.setParameter("email", usuario.getEmail())
			.executeUpdate();
		
		String qlStringPerfil = "UPDATE Perfil p SET p.nome=:nome WHERE p.id=:id";
		int executeUpdate = this.em.createQuery(qlStringPerfil)
				.setParameter("nome", perfil.getNome())
				.setParameter("id", perfil.getId())
			.executeUpdate();
		
		return executeUpdate;
	}

}
