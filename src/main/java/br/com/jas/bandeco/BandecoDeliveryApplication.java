package br.com.jas.bandeco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

@SpringBootApplication
@EnableGlobalMethodSecurity(prePostEnabled=true,securedEnabled=true)
public class BandecoDeliveryApplication {

	public static void main(String[] args) {
		SpringApplication.run(BandecoDeliveryApplication.class, args);
	}
}
