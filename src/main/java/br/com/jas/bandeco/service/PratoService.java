package br.com.jas.bandeco.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jas.bandeco.model.Prato;
import br.com.jas.bandeco.repository.PratoRepository;

@Service
public class PratoService {

	@Autowired
	private PratoRepository repository;
	
	public PratoService() { }
	
	/* ********************** */
	/* ***** REPOSITORY ***** */
	/* ********************** */
	
	public List<Prato> findAll() {
		return this.repository.findAll();
	}
	
	public Prato save(Prato prato) {
		return this.repository.saveAndFlush(prato);
	}
	
	public Prato findById(Long id) {
		return this.repository.findById(id).orElse(null);
	}
}
