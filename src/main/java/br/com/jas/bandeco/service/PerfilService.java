package br.com.jas.bandeco.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import br.com.jas.bandeco.model.Perfil;
import br.com.jas.bandeco.model.Usuario;
import br.com.jas.bandeco.repository.PerfilRepository;
import br.com.jas.bandeco.representacao.FormAlterarSenha;
import br.com.jas.bandeco.validator.FormAlterarSenhaValidator;
import br.com.jas.bandeco.validator.PerfilValidator;

@Service
public class PerfilService {

	@Autowired
	private PerfilRepository repository;
	
	@Autowired
	private PerfilValidator validator;
	
	@Autowired
	private FormAlterarSenhaValidator formAlterarSenhaValidator;
	
	@Autowired
	private UsuarioService usuarioService;
	
	/* ================================ *
	 * ========== REPOSITORY ========== *
	 * ================================ *
	 */
	
	public List<Perfil> findAll() {
		return this.repository.findAll();
	}
	
	public Perfil save(Perfil perfil) {
		return this.repository.save(perfil);
	}
	
	public Perfil findById(Long id) {
		Optional<Perfil> findById = this.repository.findById(id);
		Perfil perfil = findById.orElse(null);
		
		return perfil;
	}

	public Perfil findByEmail(String email) {
		Perfil perfil = this.repository.findByEmail(email);
		
		return perfil;
	}

	
	public Integer editar(Perfil perfil) {
		Integer i = this.repository.editar(perfil);
		
		return i;
	}
	
	public Integer atualizarSenha(String novaSenha, String email) {
		return this.usuarioService.atualizarSenha(novaSenha, email);
	}
	
	/* =============================== *
	 * ========== VALIDATOR ========== *
	 * =============================== *
	 */
	
	/**
	 * 
	 * @param target
	 * @param errors
	 */
	public void validate(Perfil target, Errors errors) {
		this.validator.validate(target, errors);
	}

	/**
	 * 
	 * @param target
	 * @param errors
	 */
	public void validateFormAlterarSenha(FormAlterarSenha target, Errors errors) {
		this.formAlterarSenhaValidator.validate(target, errors);
	}
	
	/* ==================================== *
	 * ========== USUARIOSERVICE ========== *
	 * ==================================== *
	 */
	
	/**
	 * 
	 * @param usuario
	 */
	public void encodeSenha(Usuario usuario) {
		this.usuarioService.encodeSenha(usuario);
	}
	
}
